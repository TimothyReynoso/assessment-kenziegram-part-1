const express = require('express');
const multer = require('multer');
const path = require('path');
const ejs = require('ejs');
const { uuid } = require('uuidv4');
const fs = require('fs');

const hostname = "localhost"
//selecting our port 
const port = 3001
//init server p1
const app = express();
// LETS DEAL WITH THE FILE SYSTEM:
const pathTo = './public/uploads';

const uploadsArray = [];
// fs.readdir(pathTo, (err, items) => {
//     console.log(items);
//     res.send(`<h1>Welcom</h1>`)
// })
// END OF FILE SYSTEM.

// LETS HANDLE MULTER HERE:
//
// Multer: set storage engine DiskStorage
const storage = multer.diskStorage({
    destination: './public/uploads',
    filename: function (req, file, cb) {
      cb(null, file.fieldname + '-' + uuid() + path.extname(file.originalname) )
    }
  })

// Multer: init upload
const upload = multer({ 
    storage: storage,
// adding an upload limit (1000000 bits)
    //limits: {fileSize: 1000000},
// managing filtering files
    fileFilter: (req, file, cb) => {
        checkFileTypes(file, cb);
    }
}).single('userImage')

// Multer: accepting specific file types
checkFileTypes = (file, cb) => {
    //varible to hold our file types via ext .xxxx
    const fileTypes = /jpeg|gif|png|jpg/;
    //Check ext
    const extname = fileTypes.test(
        path.extname(file.originalname).toLowerCase()
    );
    //Check mime 
    const mimetype = fileTypes.test(file.mimetype)
    //making sure extname and mimetype are true
    if(extname && mimetype){
        return cb(null,true);
    } else {
        cb(`Error: Images Only`);
    }
}
// DONE WITH MULTER FOR FILE UPLOAD FUNCATIONALITY.

//EJS
app.set('view engine', 'ejs');
//let's let express that /public is our root file (.use == middleware)
app.use(express.static('./public'));

//HTTP REQUEST METHODS START:

//POST - request on submit
app.post('/uploadPic', (req, res) => {
    upload(req, res, (err) => {
       //console.log(req)
       //error handling
       if(err){
           res.render('index', {
               msg: err
           });
       } else if (req.file === undefined) {
           res.render('index', {
               msg: `Error: NO File Selected`
           }) 
       } else {
            uploadsArray.push(req.file.filename)
            //console.log(req.file.filename)
            //console.log(uploadsArray)
            const justUploaded = `./uploads/${uploadsArray[uploadsArray.length-1]}`
           // console.log(images);
           res.render('completeUpload', {
               msg: `File Uploaded`,
               file: justUploaded
           }
           , 
            )//`uploads/${req.file.filename}` ---- `<img src=${photo}>`
       }// items.map((photo) => `uploads/${photo}`)
   })

})

//    , () => {fs.readdir(pathTo, (err, items) => {
        //         console.log(items);
        //         res.send(items.map((photo) => `<img src=${photo}>`))
        //     })}


//.getElementById('image').src = "uploads/userImage-0a115a34-60d3-48e9-95d1-66296af3d451.jpg"
//root GET request, using ejs from the index.ejs file
app.get('/', (req, res) =>  {
    fs.readdir(pathTo, (err, items) => {
        const images = items.map((image) => {
        return `./uploads/${image}`
    })
        res.render('index', {
            photo: images

        })
    })
     

    });

//HTTP REQUEST METHODS END.

app.listen( port,()=>{console.log(`Listing at Port: ${port}`)});